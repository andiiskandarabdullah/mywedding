<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Vie & Andi &mdash; Wedding</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Wedding Day | Andi & Evi" />
	<meta name="keywords" content="wedding" />
	<meta name="author" content="TRILOGI ART PROJECT" />

  <!--
	//////////////////////////////////////////////////////

	FREE HTML5 TEMPLATE
	DESIGNED & DEVELOPED by QBOOTSTRAP.COM

	Website: 		http://qbootstrap.com/
	Email: 			info@qbootstrap.com
	Twitter: 		http://twitter.com/Q_bootstrap
	Facebook: 		https://www.facebook.com/Qbootstrap
	//////////////////////////////////////////////////////
	 -->

  	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
	<link rel="shortcut icon" href="favicon.ico">
    <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css')}}">
	<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,600,400italic,700' rel='stylesheet' type='text/css'>
	<link href="https://fonts.googleapis.com/css?family=Clicker+Script" rel="stylesheet">

	<!-- Animate.css -->
	<link rel="stylesheet" href="{{ asset('plugins/css/animate.css')}}">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="{{ asset('plugins/css/icomoon.css')}}">
	<!-- Simple Line Icons -->
	<link rel="stylesheet" href="{{ asset('plugins/css/simple-line-icons.css')}}">
	<!-- Owl Carousel -->
	<link rel="stylesheet" href="{{ asset('plugins/css/owl.carousel.min.css')}}">
	<link rel="stylesheet" href="{{ asset('plugins/css/owl.theme.default.min.css')}}">
	<!-- Magnific popup  -->
	<link rel="stylesheet" href="{{ asset('plugins/css/magnific-popup.css')}}">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="{{ asset('plugins/css/bootstrap.css')}}">
     <style>
         .active-display{
             display: block;
         }
         .unactive-display{
             display: none;
         }
        video {
            width: 100%;
            height: auto;
        }
        .img-mobile {
            width: 100%;
            height: auto;
        }
     </style>
	<link rel="stylesheet" href="{{ asset('plugins/css/style.css')}}">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
    integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
    crossorigin=""/>
	<!-- Modernizr JS -->
	<script src="{{ asset('plugins/js/modernizr-2.6.2.min.js')}}"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="{{ asset('plugins/js/respond.min.js')}}"></script>
	<![endif]-->
	</head>
	<body>
        <div class="modal fade" id="modal1">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Pertemuan Pertama</h4>
                    </div>
                    <div class="modal-body">
                        <span class="date">Desember 11, 2012</span>
                        <p>Cinta adalah kekuatan yang tidak akan pernah berakhir kisahnya, dan semua orang pasti memiliki kisah cinta yang berbeda. Pertemuan pertama yang tidak pernah diduga akan menjadi sebuah kisah cinta, ketika duduk di bangku SMP. tidak banyak kisah yang kita ukir, beranjak remaja seketika berubah menjadi penantian. Delapan tahun bukan waktu yang sebentar untuk sebuah penantian. akan tetapi disitulah kisah cinta kita bermula.
                        </p>
                    </div>
                    <div class="modal-footer justify-content-between float-right">
                        <button class="btn btn-sm btn-default float-right" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modal2">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Kesempatan Kedua</h4>
                    </div>
                    <div class="modal-body">
                        <span class="date">November 13, 2017</span>
                        <p>Waktu ke waktu terus dilalui dengan penantian yang selalu menghantui, saling lupa dengan kisah yang berbeda, berhenti menanti dari penantian yang tak pasti.

                            Tetapi takdir berkata lain, selalu ada waktu untuk kita bisa bertemu, tidak diduga dan tidak disangka akan ada kesempatan kedua.

                           Pertemuan yang secara tidak langsung mengingat akan sebuah kisah sebelumnya, dan mulai mengukir kembali penantian yang sempat tak pasti.
                        </p>
                    </div>
                    <div class="modal-footer justify-content-between float-right">
                        <button class="btn btn-sm btn-default float-right" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modal3">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Saling Memantaskan Diri</h4>
                    </div>
                    <div class="modal-body">
                        <span class="date">Februari 29, 2020</span>
                        <p>Sejak awal pertemuan ketika beranjak remaja, hingga merasakan kembali perasaan yang dulu ketika sudah merasa dewasa. tentu dengan situasi yang berbeda, karena kita saling beranggapan tidak lagi memiliki rasa yang sama.

                            Merasa tidak layak untuk kembali dan tidak mungkin untuk melanjutkan apa yang dulu pernah diperjuangkan.

                            Akan tetapi lagi dan lagi takdir berkata lain, semenjak pertemuan yang tidak direncanakan. terjalin komunikasi untuk meyakinkan kembali, hingga akhirnya kita saling memantaskan diri.
                        </p>
                    </div>
                    <div class="modal-footer justify-content-between float-right">
                        <button class="btn btn-sm btn-default float-right" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modal4">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Khitbah</h4>
                    </div>
                    <div class="modal-body">
                        <span class="date">Sept. 09, 2020</span>
                        <p>Inilah kekuatan cinta yang secara tidak langsung kita rasakan. merubah sebuah penantian menjadi kisah cinta yang kita ukir untuk masa depan.

                            delapan tahun yang dilalui menjadi kisah yang menarik untuk kita saling menceritakan kembali dan tak pernah menyangka bisa sampai seperti ini.

                            Hingga kita sadari semua kisah cinta yang kita lalui ini tak akan berarti lagi jika tidak berakhir dengan janji yang suci untuk membangun sebuah hubungan yang direstui dan diridhoi.
                        </p>
                    </div>
                    <div class="modal-footer justify-content-between float-right">
                        <button class="btn btn-sm btn-default float-right" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
	<header role="banner" id="qbootstrap-header">
		<div class="container">

			<!-- <div class="row"> -->
		    <nav class="navbar navbar-default">
	        <div class="navbar-header">
	        	<!-- Mobile Toggle Menu Button -->
				<a href="#" class="js-qbootstrap-nav-toggle qbootstrap-nav-toggle" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"><i></i></a>
	          	<a class="navbar-brand" href="index.html">Wedding</a>
	        </div>
	        <div id="navbar" class="navbar-collapse collapse">
	          <ul class="nav navbar-nav navbar-right">
	            <li class="active"><a href="#" data-nav-section="home"><span>Home</span></a></li>
	            <li><a href="#" data-nav-section="groom-bride"><span>Vie &amp; Andi</span></a></li>
	            <li><a href="#" data-nav-section="story"><span>Love Story</span></a></li>
	            <li><a href="#" data-nav-section="when-where"><span>When &amp; Where</span></a></li>
	            <li><a href="#" data-nav-section="rsvp"><span>RSVP</span></a></li>
	            <li><a href="#" data-nav-section="gallery"><span>Gallery</span></a></li>
	          </ul>
	        </div>
		    </nav>
		  <!-- </div> -->
	  </div>
	</header>

	<div class="qbootstrap-hero" data-section="home">
		<div class="qbootstrap-overlay"></div>
		<div class="qbootstrap-cover text-center img-mobile" data-stellar-background-ratio="0.5" style="background-image: url({{ asset('images/IMG_6413.jpg')}});">
			<div class="display-t">
				<div class="display-tc">
					<div class="container">
						<div class="col-md-10 col-md-offset-1">
							<div class="animate-box svg-sm colored">
								<img src="{{ asset('plugins/images/flaticon/svg/004-nature.svg')}}" class="svg" alt="Vie & Andi">
								<h1 class="holder"><span>The Wedding of</span></h1>
								<h2> Vie &amp; Andi </h2>
								<p>04.04.2021</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
    <div class="elementor-widget-container">
        <div class="elementor-shortcode"><!-- Music -->
            <!-- strat music-box -->
            <div class="music-box">
            <button class="music-box-toggle-btn">
            <!--
            <i class="fa fa-music"></i> -->
            <audio id="song" autoplay loop>
            <source src="{{asset('lagunya.mp3')}}">
            </audio>

            </button><button type="button" class="music" id="mute-sound" style="display: none;">
            <i class="fas fa-music"></i>
            </button>
            <button type="button" class="music" id="unmute-sound" style="display: inline-block;">
            <i class="fas fa-microphone-slash"></i>
            </button>

            </div>

            <!-- end music box -->
	    </div>
    </div>
	<div id="qbootstrap-couple" class="qbootstrap-section-gray">
		<div class="container">
			<div class="row animate-box">
				<div class="col-md-8 col-md-offset-2 animate-box">
					<div class="col-md-12 text-center section-heading svg-sm colored">
						<img src="{{ asset('plugins/images/flaticon/svg/005-two.svg')}}" class="svg" alt="Vie & Andi">
						<h2>Are Getting Married</h2>
						<p>Ketika dua hati menyatu, Allah-lah yang mempertemukannya. Tatkala janji terucap, Allah-lah menjadi saksinya. Maha suci Allah yang telah menjadikan segala sesuatunya indah dan sempurna.</p>
						<p><strong>Minggu, 04 April 2021 &mdash; Karangtengah, Cianjur</strong></p>
					</div>
				</div>
			</div>
			<div class="row animate-box">
				<div class="col-md-8 col-md-offset-2 text-center">
					<div class="col-md-5 col-sm-5 col-xs-5 nopadding">
						<img src="{{ asset('images/evi2.jpg')}}" class="img-responsive" alt="Vie & Andi">
						<h3> Evi Silviani</h3>
						<span>Vie</span>
					</div>
					<div class="col-md-2 col-sm-2 col-xs-2 nopadding"><h2 class="amp-center"><img src="{{ asset('plugins/images/flaticon/svg/003-luxury.svg')}}" class="svg img-responsive" alt="Vie & Andi"></h2></div>
					<div class="col-md-5 col-sm-5 col-xs-5 nopadding">
						<img src="{{ asset('images/andi2.jpg')}}" class="img-responsive" alt="Vie & Andi">
						<h3> Andi Iskandar Abdullah</h3>
						<span>Andi</span>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="qbootstrap-countdown" data-stellar-background-ratio="0.5" style="background-image: url({{ asset('images/IMG_6409.jpg')}});" data-section="wedding-day">
		<div class="overlay"></div>
		<div class="display-over">
			<div class="container">
				<div class="row animate-box">
					<div class="col-md-12 section-heading text-center svg-sm colored">
						<img src="{{ asset('plugins/images/flaticon/svg/006-flower-bell-outline-design-variant-with-vines-and-leaves.svg')}}" class="svg" alt="Vie & Andi">
						<h2 class="">The Wedding Day</h2>
						<span class="datewed">Minggu, 04 April 2021</span>
					</div>
				</div>
				<div class="row animate-box">
					<div class="col-md-8 col-md-offset-2 text-center">
						<p class="countdown">
							<span id="days"></span>
							<span id="hours"></span>
							<span id="minutes"></span>
							<span id="seconds"></span>
							<span id="demo"></span>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="qbootstrap-groom-bride" data-section="groom-bride">
		<div class="container">
			<div class="row animate-box">
				<div class="col-md-8 col-md-offset-2">
					<div class="col-md-12 text-center section-heading svg-sm-2 colored">
						<h3>"Dan nikahkanlah orang-orang yang sendirian di antara kamu, dan orang-orang yang layak (menikah) dari hamba sahayamu yang lelaki dan hamba-hamba sahayamu yang perempuan. jika mereka miskin Allah akan mengkayakan mereka dengan karunianya. Dan Allah Maha Luas (pemberiannya) dan Maha Mengetahui."</h3>
                        <p><strong> (QS. An Nuur (24) : 32)</strong></p>
                    </div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="couple bride  text-center animate-box">
						<img src="{{ asset('images/evi2.jpg')}}" class="img-responsive" alt="Vie & Andi">
						<div class="desc">
							<h2> Evi Silviani </h2>
							<p><small><i>Putri Kedua dari Bapak Miftah (alm) &amp; Ibu Euis Jamilah</i></small></p>
							<ul class="social social-circle">
								<li><a target="_blank" href="https://www.facebook.com/vie.zm.1/"><i class="icon-facebook"></i></a></li>
								<li><a target="_blank" href="https://www.instagram.com/evisilviani09/?hl=id"><i class="icon-instagram"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="couple groom text-center animate-box">
						<img src="{{ asset('images/andi2.jpg')}}" class="img-responsive" alt="Vie & Andi">
						<div class="desc">
							<h2> Andi Iskandar Abdullah</h2>
							<p><small><i>Putra Kelima dari Bapak Enceng &amp; Ibu Maah</i></small> </p>
							<ul class="social social-circle">
								<li><a target="_blank" href="https://www.facebook.com/andiiskandar.abdoellahpartii/"><i class="icon-facebook"></i></a></li>
								<li><a target="_blank" href="https://www.instagram.com/andiskandarrr/?hl=id"><i class="icon-instagram"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="qbootstrap-story" data-section="story">
		<div class="container">
			<div class="row animate-box">
				<div class="col-md-8 col-md-offset-2">
					<div class="col-md-12 text-center section-heading svg-sm-2">
						<img src="{{ asset('plugins/images/flaticon/svg/003-luxury.svg')}}" class="svg" alt="Vie & Andi">
						<h2>Our Love Story</h2>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<ul class="timeline animate-box">
						<li class="animate-box">
							<div class="timeline-badge" style="background-image:url({{ asset('images/IMG_5737.jpg')}});"></div>
							<div class="timeline-panel">
								<div class="overlay"></div>
								<div class="timeline-heading">
									<h3 class="timeline-title">Pertemuan Pertama</h3>
									<span class="date">Desember 11, 2012</span>
								</div>
								<div class="timeline-body">
									<p style="text-align: justify">Cinta adalah kekuatan yang tidak akan pernah berakhir kisahnya, dan semua orang pasti memiliki kisah cinta yang berbeda. <a href="#" class="btn btn-sm btn-outline-primary" data-toggle="modal" data-target="#modal1">Read more ..</a></p>
								</div>
							</div>
						</li>
						<li class="timeline-inverted animate-box">
							<div class="timeline-badge" style="background-image:url({{ asset('images/IMG_57591.jpg')}});"></div>
							<div class="timeline-panel">
								<div class="overlay overlay-2"></div>
								<div class="timeline-heading">
									<h3 class="timeline-title">Kesempatan Kedua</h3>
									<span class="date">November 13, 2017</span>
								</div>
								<div class="timeline-body">
									<p style="text-align: justify">Waktu ke waktu terus dilalui dengan penantian yang selalu menghantui, saling lupa dengan kisah yang berbeda,  <a href="#" class="btn btn-sm btn-outline-primary" data-toggle="modal" data-target="#modal2">Read more ..</a></p>
								</div>
							</div>
						</li>
						<li class="animate-box">
							<div class="timeline-badge" style="background-image:url({{ asset('images/IMG_6429.jpg')}});"></div>
							<div class="timeline-panel">
								<div class="overlay"></div>
								<div class="timeline-heading">
									<h3 class="timeline-title">Saling Memantaskan Diri</h3>
									<span class="date">Februari 29, 2020</span>
								</div>
								<div class="timeline-body">
									<p style="text-align: justify">Sejak awal pertemuan ketika beranjak remaja, hingga merasakan kembali perasaan yang dulu ketika sudah merasa dewasa.  <a href="#" class="btn btn-sm btn-outline-primary" data-toggle="modal" data-target="#modal3">Read more ..</a></p>
								</div>
							</div>
						</li>
						<li class="timeline-inverted animate-box">
							<div class="timeline-badge" style="background-image:url({{ asset('images/IMG_6409.jpg')}});"></div>
							<div class="timeline-panel">
								<div class="overlay overlay-2"></div>
								<div class="timeline-heading">
									<h3 class="timeline-title">Khitbah</h3>
									<span class="date">Sept. 09, 2020</span>
								</div>
								<div class="timeline-body">
									<p style="text-align: justify">Inilah kekuatan cinta yang secara tidak langsung kita rasakan. merubah sebuah penantian menjadi kisah cinta yang kita ukir untuk masa depan. <a href="#" class="btn btn-sm btn-outline-primary" data-toggle="modal" data-target="#modal4">Read more ..</a></p>
								</div>
							</div>
						</li>
			    	</ul>
				</div>
			</div>
		</div>
	</div>

	<div id="qbootstrap-when-where" data-section="when-where">
		<div class="container">
			<div class="row animate-box">
				<div class="col-md-8 col-md-offset-2">
					<div class="col-md-12 text-center section-heading svg-sm colored">
						<img src="{{ asset('plugins/images/flaticon/svg/005-two.svg')}}" class="svg" alt="Vie & Andi">
						<h2>Save The Date</h2>
						<div class="row">
						<div class="col-md-10 col-md-offset-1 subtext">
							<h3>Rasa haru dan bahagia terukir dihati kami
                                dan atas limpahan Rahmat Allah SWT,
                                kami bersimpuh memohon Ridho-Nya
                                untuk melakukan Resepsi Pernikahan
                                yang insya Allah akan dilaksanakan pada :</h3>
						</div>
					</div>
					</div>
				</div>
			</div>
			<div class="row row-bottom-padded-md">
                <div class="col-md-2">
                </div>
				<div class="col-md-8 text-center animate-box">
					<div class="wedding-events">
						<div class="ceremony-bg" style="background-image: url({{ asset('images/IMG_5736.jpg')}}); height:200px;"></div>
						<div class="desc">
							<h3>Wedding Ceremony</h3>
							<div class="row">
								<div class="col-md-2 col-md-push-5">
									<div class="icon-tip">
										<span class="icon"><i class="icon-heart-o"></i></span>
									</div>
								</div>
								<div class="col-md-5 col-md-pull-1">
									<div class="date">
										<i class="icon-calendar"></i>
										<span>Minggu</span>
										<span>04 April. 2021</span>
									</div>
								</div>
								<div class="col-md-5 col-md-pull-1">
									<div class="date">
										<i class="icon-clock2"></i>
										<span><strong>Resepsi</strong>  09:00 AM</span>
									</div>
								</div>
                            </div>
                            <br>
                            <br>
                                <p>Tiada kata yang pantas kami haturkan
                                    selain ucapan terima kasih yang tak terhigga
                                    atas kehadiran dan doa restu Bapak/Ibu/Saudara/i.</p>
						</div>
					</div>
                </div>
                <div class="col-md-2">
                </div>

            </div>
            <div class="row">
                <div class="col-md-12 text-center animate-box">
                    <div class="wedding-events">
                        <div class="desc">
                            <h3>Location</h3>
                            <p>Kp. Cijurang RT/RW 06/06, Desa Hegarmanah, Kecamatan Karangtengah, Kabupaten Cianjur</p>
                            <p><a target="_blank" href="https://goo.gl/maps/wEFxyw8Nmw3W5u3f8" class="btn btn-primary btn-sm">Detail Location</a></p>
                            <div id="mapid" style="height: 330px; width:100%"></div>

                        </div>
                    </div>
                </div>
            </div>
		</div>
		</div>
	</div>
	<div id="qbootstrap-started" class="qbootstrap-bg" data-section="rsvp" data-stellar-background-ratio="0.5" style="background-image: url({{ asset('images/IMG_6246.jpg')}});">
		<div class="overlay"></div>
		<div class="container">
			<div class="row animate-box">
				<div class="col-md-8 col-md-offset-2">
					<div class="col-md-12 text-center section-heading svg-sm colored">
						<img src="{{ asset('plugins/images/flaticon/svg/005-two.svg')}}" class="svg" alt="Vie & Andi">
						<h2>You Are Invited</h2>
						<div class="row">
						<div class="col-md-10 col-md-offset-1 subtext">
							<h3>Besar harapan kami kepada Bapak/Ibu/Saudara/i untuk bisa menghadiri atau meluangkan waktu nya. </h3>
						</div>
					</div>
					</div>
				</div>
			</div>
			<div class="row animate-box">
				<div class="col-md-12" id="form-rsvp">
					<form class="form-inline" action="{{route('rsvp')}}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1">
                                <div class="col-md-4 col-sm-4">
                                    <div class="form-group">
                                        <label style="color: white">Nama</label>
                                        <input type="name" class="form-control" id="name" name="name" placeholder="Name">
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <div class="form-group">
                                        <label style="color: white">Email</label>
                                        <input type="email" class="form-control" id="email" name="email" placeholder="Email">
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <div class="form-group">
                                        <label style="color: white">Berapa orang</label>
                                        <input type="number" class="form-control" id="guest" name="guest" placeholder="Guest">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-10 col-md-offset-1">
                                <div class="col-md-12 col-sm-4">
                                    <label style="color: white">Ucapan dan Doa</label>
                                    <textarea name="messages" class="form-control" id="doa" cols="300" rows="10"></textarea>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1">
                                <div class="col-md-4 col-sm-4">
                                    <button type="submit" class="btn btn-default btn-block" id="submit-rsvp">I am Attending</button>
                                </div>
                            </div>
                        </div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<div id="qbootstrap-gallery" data-section="gallery">
		<div class="container">
			<div class="row animate-box">
				<div class="col-md-8 col-md-offset-2">
					<div class="col-md-12 text-center section-heading svg-sm colored">
						<img src="{{ asset('plugins/images/flaticon/svg/005-two.svg')}}" class="svg" alt="Vie & Andi">
						<h2>Our Selfie Photos</h2>
						<div class="row">
						<div class="col-md-10 col-md-offset-1 subtext">
							<h3>Mengabadikan moment disetiap kisah yang telah dilalui, tidak ada lagi pertemuan yang lebih berkesan melainkan bisa bersatu kembali dengan ikatan cinta yang sesungguhnya.</h3>
						</div>
					</div>
					</div>
				</div>
			</div>
		</div>

		<div class="container-fluid">
			<div class="row">
				<div class="col-md-3 col-sm-6">
					<div class="gallery animate-box">
						<a class="gallery-img image-popup" href="{{ asset('images/IMG_6413.jpg')}}"><img src="{{ asset('images/IMG_6413.jpg')}}" class="img-responsive" alt="Vie & Andi"></a>
					</div>
					<div class="gallery animate-box">
						<a class="gallery-img image-popup" href="{{ asset('images/IMG_5758.jpg')}}"><img src="{{ asset('images/IMG_5758.jpg')}}" class="img-responsive" alt="Vie & Andi"></a>
                    </div>
                    <div class="gallery animate-box">
						<a class="gallery-img image-popup" href="{{ asset('images/IMG_5759.jpg')}}"><img src="{{ asset('images/IMG_5759.jpg')}}" class="img-responsive" alt="Vie & Andi"></a>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="gallery animate-box">
						<a class="gallery-img image-popup" href="{{ asset('images/IMG_5754.jpg')}}"><img src="{{ asset('images/IMG_5754.jpg')}}" class="img-responsive" alt="Vie & Andi"></a>
					</div>
					<div class="gallery animate-box">
						<a class="gallery-img image-popup" href="{{ asset('images/IMG_5741.jpg')}}"><img src="{{ asset('images/IMG_5741.jpg')}}" class="img-responsive" alt="Vie & Andi"></a>
					</div>
					<div class="gallery animate-box">
						<a class="gallery-img image-popup" href="{{ asset('images/IMG_5757.jpg')}}"><img src="{{ asset('images/IMG_5757.jpg')}}" class="img-responsive" alt="Vie & Andi"></a>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="gallery animate-box">
						<a class="gallery-img image-popup" href="{{ asset('images/IMG_6515.jpg')}}"><img src="{{ asset('images/IMG_6515.jpg')}}" class="img-responsive" alt="Vie & Andi"></a>
					</div>
					<div class="gallery animate-box">
						<a class="gallery-img image-popup" href="{{ asset('images/IMG_6530.jpg')}}"><img src="{{ asset('images/IMG_6530.jpg')}}" class="img-responsive" alt="Vie & Andi"></a>
					</div>
					<div class="gallery animate-box">
						<a class="gallery-img image-popup" href="{{ asset('images/IMG_6419.jpg')}}"><img src="{{ asset('images/IMG_6419.jpg')}}" class="img-responsive" alt="Vie & Andi"></a>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="gallery animate-box">
						<a class="gallery-img image-popup" href="{{ asset('images/IMG_5746.jpg')}}"><img src="{{ asset('images/IMG_5746.jpg')}}" class="img-responsive" alt="Vie & Andi"></a>
					</div>
					<div class="gallery animate-box">
						<a class="gallery-img image-popup" href="{{ asset('images/IMG_6532.jpg')}}"><img src="{{ asset('images/IMG_6532.jpg')}}" class="img-responsive" alt="Vie & Andi"></a>
                    </div>
                    <div class="gallery animate-box">
						<a class="gallery-img image-popup" href="{{ asset('images/IMG_6429.jpg')}}"><img src="{{ asset('images/IMG_6429.jpg')}}" class="img-responsive" alt="Vie & Andi"></a>
					</div>
				</div>
			</div>

		</div>
	</div>
    <div id="qbootstrap-started" class="qbootstrap-bg" data-section="rsvp" data-stellar-background-ratio="0.5" style="background-color: white">
		<div class="overlay"></div>
		<div class="container">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 text-center">
                        <div class="row animate-box">
                            <div class="wedding-events gallery">
                                <video controls>
                                    <source src="{{ asset('images/video.mp4')}}" type="video/mp4">
                                </video>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

	<footer id="footer" role="contentinfo">
		<div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="row animate-box">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="col-md-12 text-center section-heading svg-sm-2 colored">
                                <img src="{{ asset('images/covid.png')}}">
                                <p><strong>JAGAN LUPA PATUHI PROTOKOL KESEHATAN</strong></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<div class="row row-bottom-padded-sm">
				<div class="col-md-12">
					<p class="copyright text-center">&copy; 2020 <a href="#">Wedding</a>. All Rights Reserved. TRILOGI ART PROJECT</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 text-center">
					<ul class="social social-circle">
						<li><a href="#"><i class="icon-twitter"></i></a></li>
						<li><a href="#"><i class="icon-facebook"></i></a></li>
						<li><a href="#"><i class="icon-youtube"></i></a></li>
						<li><a href="#"><i class="icon-instagram"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
	</footer>


	<!-- jQuery -->
	<script src="{{ asset('plugins/js/jquery.min.js')}}"></script>
	<!-- jQuery Easing -->
	<script src="{{ asset('plugins/js/jquery.easing.1.3.js')}}"></script>
	<!-- Bootstrap -->
	<script src="{{ asset('plugins/js/bootstrap.min.js')}}"></script>
	<!-- Waypoints -->
	<script src="{{ asset('plugins/js/jquery.waypoints.min.js')}}"></script>
	<!-- YTPlayer -->
	<script src="{{ asset('plugins/js/jquery.mb.YTPlayer.min.js')}}"></script>
	<!-- Flexslider -->
	<script src="{{ asset('plugins/js/jquery.flexslider-min.js')}}"></script>
	<!-- Owl Carousel -->
	<script src="{{ asset('plugins/js/owl.carousel.min.js')}}"></script>
	<!-- Parallax -->
	<script src="{{ asset('plugins/js/jquery.stellar.min.js')}}"></script>
	<!-- Magnific Popup -->
	<script src="{{ asset('plugins/js/jquery.magnific-popup.min.js')}}"></script>
	<script src="{{ asset('plugins/js/magnific-popup-options.js')}}"></script>
	<!-- Main JS (Do not remove) -->
	<script src="{{ asset('plugins/js/main.js')}}"></script>
     <!-- Make sure you put this AFTER Leaflet's CSS -->
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
    integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
    crossorigin=""></script>

    <script>
        var mymap = L.map('mapid').setView([-6.812241, 107.187054], 14);
        L.tileLayer(
                "https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw",
                {
                    maxZoom: 18,
                    attribution:
                    'Location Wedding Evi & Andi',
                    id: "mapbox/streets-v11",
                    tileSize: 512,
                    zoomOffset: -1
                }
            ).addTo(mymap);
            var marker = L.marker([-6.812241, 107.187054]).addTo(mymap);
            marker.bindPopup("Location Wedding <br> Vie & Andi").openPopup();
    </script>
    <script>
        document.getElementById('mute-sound').style.display = 'none';
        document.getElementById('unmute-sound').addEventListener('click', function(event){
            document.getElementById('unmute-sound').style.display = 'none';
            document.getElementById('mute-sound').style.display = 'inline-block';
            document.getElementById('song').play();
        });

        document.getElementById('mute-sound').addEventListener('click', function(event){
            document.getElementById('mute-sound').style.display = 'none';
            document.getElementById('unmute-sound').style.display = 'inline-block';
            document.getElementById('song').pause();
        });


        </script>
        <script>
            $('#submit-rsvp').click(function() {
                sessionStorage.setItem('rsvp', true);
            });
            $(document).ready(function () {
                var rsvp = sessionStorage.getItem('rsvp');
                console.log(rsvp);
                if (rsvp == 'true') {
                    console.log('rsvp');
                    $('#form-rsvp').hide();
                }else{
                    console.log('wkwk');
                    $('#form-rsvp').show();
                }
            })
        </script>
	</body>
</html>
