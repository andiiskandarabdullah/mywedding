<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rsvp extends Model
{
    use HasFactory;

    protected $table ='rsvp';
    protected $fillable = ['name','email','guest','messages'];
    protected $dates = ['created_at'];
}
